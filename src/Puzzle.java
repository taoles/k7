import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Collections;

public class Puzzle {
   static ArrayList<Character> uniquechar = new ArrayList<Character>();
   static HashSet<String> res = new HashSet<String>();
   static int nos[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
   static HashMap<Character, Integer> hm = new HashMap<Character, Integer>();
   static long no1, no2, no3 = 0;
   static ArrayList<ArrayList<Integer>> permuts = new ArrayList<ArrayList<Integer>>();
   static String s1, s2, s3;

   /**
    * Solve the word puzzle.
    *
    * @param args three words (addend1 addend2 sum)
    */
   public static void main(String[] args) {
      uniquechar.clear();
      res.clear();
      hm.clear();
      permuts.clear();

      s1 = args[0];
      s2 = args[1];
      s3 = args[2];
      addToArrayList(s1);
      addToArrayList(s2);
      addToArrayList(s3);

      System.out.printf("Input is \"%s + %s = %s\"\n", s1, s2, s3);
      long start = System.currentTimeMillis();
      calculate();
      long end = System.currentTimeMillis();
      double time = (end - start) / 1000.0;
      if (res.isEmpty()) {
         System.out.println("No solutions!");
      } else if (res.size() == 1) {
         System.out.println("Puzzle has 1 solution");
         System.out.println("Solution: " + getMapFromString(res.iterator().next()));

      }
      else {
         System.out.printf("Puzzle has %s solutions\n", res.size());
         System.out.println("One of the solutions: \n" + getMapFromString(res.iterator().next()));
      }

      System.out.println("Time required for execution: " + time + " seconds\n\n");
   }

   /**
    * Find all permutations from uniquechar array and add to res if permutation is valid for this equation.
    */
   public static void calculate() {
      Collections.sort(uniquechar);
      permute(nos, 0);

      for (int i = 0; i < permuts.size(); i++) {
         for (int j = 0; j < uniquechar.size(); j++) {
            hm.put(uniquechar.get(j), permuts.get(i).get(j));
         }

         no1 = getNumber(s1);
         no2 = getNumber(s2);
         no3 = getNumber(s3);

         if (no3 == no1 + no2 && getLengthOfLong(no1) == s1.length() && getLengthOfLong(no2) == s2.length() && getLengthOfLong(no3) == s3.length()) {
            res.add(s1 + ": " + no1 + ", " + s2 + ": " + no2 + ", " + s3 + ": " + no3);
         }
      }

   }


   /**
    * Recursive method to find all permutations and put them to permuts list.
    * @param a
    * @param k
    */
   public static void permute(int[] a, int k) {
      if (k == a.length) {
         ArrayList<Integer> perm = new ArrayList<Integer>();
         for (int i = 0; i < a.length; i++) {
            perm.add(a[i]);
         }
         permuts.add(perm);
      } else {
         for (int i = k; i < a.length; i++) {
            int temp = a[k];
            a[k] = a[i];
            a[i] = temp;
            permute(a, k + 1);
            temp = a[k];
            a[k] = a[i];
            a[i] = temp;
         }
      }
   }

   /**
    * Return true if character c in uniquechar, else return false.
    * @param c
    * @return
    */
   public static boolean found(char c) {
      boolean flag = false;
      for (int i = 0; i < uniquechar.size(); i++) {
         if (uniquechar.get(i) == c)
            flag = true;
      }
      if (flag)
         return true;
      else
         return false;
   }

   /**
    * Add all string characters to uniquechar.
    * @param s
    */
   public static void addToArrayList(String s) {
      for (int i = 0; i < s.length(); i++) {
         if (!found(s.charAt(i))) {
            uniquechar.add(s.charAt(i));
         }
      }
   }


   /**
    * Change all letters from string to their values from HashMap with numeric values and return that long value.
    * @param s
    * @return
    */
   public static long getNumber(String s) {

      StringBuilder temp = new StringBuilder();
      for (int i = 0; i < s.length(); i++) {
         temp.append(hm.get(s.charAt(i)));
      }
      return Long.parseLong(temp.toString());
   }


   /**
    * Return length of given long number.
    * @param n
    * @return
    */
   public static int getLengthOfLong(long n) {
      return String.valueOf(n).length();
   }


   /**
    * Return HashMap with characters and their correct number values collected from given string.
    *
    * @param s
    * @return
    */
   public static HashMap<Character, Integer> getMapFromString(String s) {
      HashMap<Character, Integer> hashMap = new HashMap<Character, Integer>();
      String[] strings = s.split(", ");
      for (String string : strings) {
         char[] characters = string.split(": ")[0].toCharArray();
         char[] numbers = string.split(": ")[1].toCharArray();
         for (int i = 0; i < characters.length; i++) {
            hashMap.put(characters[i], Integer.parseInt(String.valueOf(numbers[i])));
         }
      }
      return hashMap;
   }
}

//Used code from: https://github.com/javedk16/Cryptarithmetic-Problem-Solver-in-Java/blob/master/CryptArithmetic.java
